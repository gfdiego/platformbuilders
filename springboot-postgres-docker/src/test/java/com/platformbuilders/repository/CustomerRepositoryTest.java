package com.platformbuilders.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Date;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.platformbuilders.model.Customer;

@RunWith(SpringRunner.class)
@DataJpaTest
public class CustomerRepositoryTest {
	@Autowired
    private TestEntityManager entityManager;
	
	@Autowired
	private CustomerRepository customerRepository;
	
	@Test
	public void shouldReturnCustomer() {
	    final Customer customer = new Customer("Diego", 123L, new Date());
	    entityManager.persist(customer);
	    entityManager.flush();
	    final Optional<Customer> customerDB = customerRepository.findByName(customer.getName());
	    assertThat(customerDB.get().getName()).isEqualTo(customer.getName());
	}

}
