package com.platformbuilders.service;


import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import java.util.Date;
import java.util.Optional;

import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.context.junit4.SpringRunner;

import com.platformbuilders.exception.CustomerRegistrationException;
import com.platformbuilders.model.Customer;
import com.platformbuilders.repository.CustomerRepository;

@RunWith(SpringRunner.class)
@ExtendWith(MockitoExtension.class)
public class CustomerServiceTest {	
	@Mock
    private CustomerRepository customerRepository;

    @InjectMocks
    private CustomerService customerService;
    
    @Test
    public void shouldSaveCustomerSuccessFully() {
        final Customer customer = new Customer("Diego", 123L, new Date());
        given(customerRepository.findByName(customer.getName())).willReturn(Optional.empty());
        given(customerRepository.save(customer)).willAnswer(invocation -> invocation.getArgument(0));
        final Customer savedCustomer = customerService.create(customer);
        assertThat(savedCustomer).isNotNull();
        verify(customerRepository).save(customer);
    }

    @Test
    public void shouldThrowErrorWhenSaveCustomerWithExistingName() {
        final Customer customer = new Customer("Diego", 123L, new Date());
        given(customerRepository.findByName(customer.getName())).willReturn(Optional.of(customer));
        assertThrows(CustomerRegistrationException.class, () -> {
            customerService.create(customer);
        });
        verify(customerRepository, never()).save(customer);
    }

    @Test
    public void shouldFindCustomerById() {
        final Integer customerId = 1;
        final Customer customer = new Customer("Diego", 123L, new Date());
        given(customerRepository.findById(customerId)).willReturn(Optional.of(customer));
        final Optional<Customer> expected = customerService.findById(customerId);
        assertThat(expected).isNotNull();
    }
}