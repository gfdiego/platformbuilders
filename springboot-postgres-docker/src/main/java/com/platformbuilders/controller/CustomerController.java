package com.platformbuilders.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.platformbuilders.model.Customer;
import com.platformbuilders.service.CustomerService;

@RestController
@RequestMapping("/api")
public class CustomerController {	
	@Autowired
	private CustomerService customerService;
	
	@GetMapping("/customers")
    public ResponseEntity<Page<Customer>> findAll(
    		@PageableDefault(sort = "name",
            direction = Sort.Direction.ASC,
            page = 0,
            size = 5) Pageable page) {
		return ResponseEntity.ok(customerService.findAll(page));
    }

    @PostMapping("/customer")
    public Customer add(@RequestBody Customer customer) {
        return customerService.create(customer);
    }

    @GetMapping("/customer/findByNameOrCpf")
    public ResponseEntity<Page<Customer>> findByNameOrCpf(
    		@RequestParam(value = "name", required = false) String name,
    		@RequestParam(value = "cpf", required = false) Long cpf,
    		@PageableDefault(sort = "name",
            direction = Sort.Direction.ASC,
            page = 0,
            size = 5) Pageable page) {
    	return ResponseEntity.ok().body(customerService.findByNameOrCpf(name, cpf, page));    	
    }
    
    @PutMapping("/customer/{id}")
    public ResponseEntity<Void> update(
    		@PathVariable(value = "id") Integer customerId,
    		@RequestBody Customer customer) {
    	customerService.update(customerId, customer);
    	return ResponseEntity.ok().build();
    }

    @PatchMapping("/customer/{id}")
    public ResponseEntity<Customer> updateName(
    		@PathVariable(value = "id") Integer customerId,
            @RequestBody Customer customer) {
    	return ResponseEntity.ok(customerService.updateName(customerId, customer));    	
    }

    @DeleteMapping("/customer/{id}")
    public ResponseEntity<Void> deleteById(@PathVariable(value = "id") Integer customerId) {
    	customerService.deleteById(customerId);
    	return ResponseEntity.ok().build();
    }
    
    @DeleteMapping("/customers")
    public ResponseEntity<Void> deleteAll() {
    	customerService.deleteAll();
		return ResponseEntity.ok().build();
    }
}
