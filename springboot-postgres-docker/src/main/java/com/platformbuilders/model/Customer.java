package com.platformbuilders.model;

import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.platformbuilders.util.DateHandler;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "Customer")
public class Customer extends CustomerAudit {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public Customer(String name, Long cpf, Date birthDate) {		
		this.name = name;
		this.cpf = cpf;
		this.birthDate = birthDate;
	}
	
	@Id
    @GeneratedValue
    private int id;
    private String name;
    private Long cpf;
    @JsonDeserialize(using = DateHandler.class)
    private Date birthDate;
    @Transient
    private int age;
    
    public int getAge() {
    	final LocalDate localBirthDate = birthDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    	final LocalDate currentDate = new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    	return Period.between(localBirthDate, currentDate).getYears();
    }
}
