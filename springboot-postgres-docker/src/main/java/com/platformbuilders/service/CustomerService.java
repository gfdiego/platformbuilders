package com.platformbuilders.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.platformbuilders.exception.CustomerNotFoundException;
import com.platformbuilders.exception.CustomerRegistrationException;
import com.platformbuilders.model.Customer;
import com.platformbuilders.repository.CustomerRepository;

@Service
public class CustomerService {
	@Autowired
    private CustomerRepository customerRepository;
	
	public Page<Customer> findAll(Pageable page) {		
		return customerRepository.findAll(page);
	}
	
	public Customer create(Customer customer) {
		Optional<Customer> customerOptional = customerRepository.findByName(customer.getName());
		if(customerOptional.isPresent())
			throw new CustomerRegistrationException("Customer with name "+ customer.getName() +" already exists");
		return customerRepository.save(customer);
	}
	
	public Page<Customer> findByNameOrCpf(String name, Long cpf, Pageable page) {
		return customerRepository.findByNameOrCpf(name, cpf, page);
	}
	
	public Optional<Customer> findById(Integer id) {
		return customerRepository.findById(id);
	}
	
	public Optional<Customer> findByName(String name) {
		return customerRepository.findByName(name);
	}
	
	public Customer update(Integer customerId, Customer customer) {
		final Customer customerDB = customerRepository.findById(customerId)
        		.orElseThrow(() -> new CustomerNotFoundException("Customer not found for this id: " + customerId));
        customerDB.setName(customer.getName());
        customerDB.setCpf(customer.getCpf());
        customerDB.setBirthDate(customer.getBirthDate());                
    	return customerRepository.save(customerDB);        
	}
	
	public Customer updateName(Integer customerId, Customer customer) {
		final Customer customerDB = customerRepository.findById(customerId)
                .orElseThrow(() -> new CustomerNotFoundException("Customer not found for this id: " + customerId));
    	customerDB.setName(customer.getName());
        return customerRepository.save(customerDB);        
	}
	
	public void deleteById(Integer customerId) {
		final Customer customer = customerRepository.findById(customerId)
    			.orElseThrow(() -> new CustomerNotFoundException("Customer not found" + customerId));
        customerRepository.delete(customer); 
	}
	
	public void deleteAll() {
		customerRepository.deleteAll();
	}
	
}
