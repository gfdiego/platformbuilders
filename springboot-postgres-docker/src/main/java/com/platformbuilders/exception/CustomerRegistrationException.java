package com.platformbuilders.exception;

public class CustomerRegistrationException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CustomerRegistrationException(String message) {
        super(message);
    }

    public CustomerRegistrationException(String message, Throwable cause) {
        super(message, cause);
    }
	
}
