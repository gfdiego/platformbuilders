package com.platformbuilders.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.platformbuilders.model.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Integer> {	
	@Query(value = "SELECT c FROM Customer c WHERE c.name = :name or c.cpf = :cpf")
	Page<Customer> findByNameOrCpf(@Param("name") String name, @Param("cpf") Long cpf, Pageable pageable);
	
	Optional<Customer> findByName(String name);
}
